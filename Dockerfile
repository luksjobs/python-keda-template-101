# Use a imagem oficial do Python
FROM python:3.10-slim

# Defina o diretório de trabalho como /app
WORKDIR /app

# Copie os arquivos necessários para o contêiner
COPY requirements.txt .
COPY app.py .

# Instale as dependências do Python especificadas no arquivo requirements.txt
RUN pip install -r requirements.txt

# Exponha a porta 8282 para que possa ser acessada externamente
EXPOSE 8282

# Comando para iniciar o aplicativo Flask
CMD ["python", "app.py"]
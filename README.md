# Python Application with KEDA Autoscaling

## Descrição

Esta é uma aplicação em Python que utiliza o KEDA (Kubernetes Event-Driven Autoscaling) para gerenciar o escalonamento automático com base em eventos. KEDA permite que a aplicação escale dinamicamente em resposta a métricas de eventos, como mensagens de fila, solicitações HTTP, e muito mais.

## Funcionalidades

- **Autoscaling com KEDA**: Ajusta automaticamente o número de réplicas da aplicação com base em eventos configurados.
- **Monitoramento de Métricas**: Utiliza métricas de eventos para determinar quando escalar a aplicação.
- **Desempenho Otimizado**: Escala horizontalmente para atender à demanda sem desperdício de recursos.

## Requisitos

- Python 3.10+
- Docker
- Kubernetes
- KEDA

## Instalação

### 1. Clone o Repositório

```bash
git clone https://gitlab.com/luksjobs/python-keda-template-101
cd python-keda-template-101
```

### 2. Criação da Imagem Docker
```bash
docker build -t python-keda:dev .
```

### 3. Rodar imagem no Docker
```bash
docker compose up -d 
```

### 4. Instalação via Helm Chart
```bash 
kubectl create namespace python-keda
helm install --set name=dev python-keda --namespace python-keda ./helm
```

### Uso

Depois que a aplicação e o KEDA estiverem configurados e em execução no Kubernetes, o KEDA começará a monitorar os eventos definidos e ajustará automaticamente o número de réplicas da aplicação com base na carga das metricas do Prometheus.


### Keda Scaled Object

Crie um ScaledObject (por padrão já tem um criado no "templates" do diretório do "helm") para definir como o KEDA deve escalar sua aplicação. Aqui está um exemplo de configuração que usa uma metrica do Prometheus como gatilho:

```yaml
apiVersion: keda.sh/v1alpha1
# Custom CRD provisionado pelo operador do Keda
kind: ScaledObject
metadata:
  name: prometheus-scaledobject
spec:
  scaleTargetRef:
    # Alvo do nosso deployment
    name: {{ include "python-ked.fullname" . }}
  # Intervalo que o Keda usa para verificar o Prometheus
  pollingInterval: 15
  # O período para aguardar após o último acionador relatado como ativo antes de dimensionar o deployment de volta para 0.
  cooldownPeriod: 30
  # Mínimo de réplicas para as quais o Keda dimensionará
  # Se você tiver um aplicativo que dependa de pubsub,
  # este seria um bom caso de uso para defini-lo como zero.
  minReplicaCount: 1
  # max replicas keda will scale to
  maxReplicaCount: 20
  advanced:
    # Configurações padrão do HPA
    # Documentação: https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/
    horizontalPodAutoscalerConfig:
      behavior:
        scaleDown:
          stabilizationWindowSeconds: 30
          policies:
            - type: Percent
              value: 50
              periodSeconds: 30
        scaleUp:
          stabilizationWindowSeconds: 0
          policies:
            - type: Percent
              value: 50
              periodSeconds: 10
  triggers:
    - type: prometheus
      metadata:
        # End-point aonde o Keda possa se comunicar com o Prometheus
        serverAddress: http://prometheus-operated.monitoring.svc:9090
        # Metrica usada para escalar nosso serviço
        metricName: http_requests_total
        # Se o limite for atingido, então o Kerda dimensionará nosso depĺoyment
        threshold: "100" 
        query: sum(rate(http_requests_total{job="python-ked-metrics-server"}[1m]))
```
from flask import Flask, request, Response
from prometheus_client import Counter, generate_latest, Histogram

app = Flask(__name__)

# Definição da métrica "http_requests_total" (conta a quantidade de requisições):
http_requests_total = Counter('http_requests_total', 'Quantidade de Requisições HTTP')

# Definição do histograma para medir a latência:
http_request_latency = Histogram('http_request_latency_seconds', 'Latência das requisições HTTP')

# Criação da rota "/metrics":
@app.route('/metrics')
def metrics():
    # Medição da latência dentro da rota /metrics:
    with http_request_latency.time():
        http_requests_total.inc()
        return Response(generate_latest(), mimetype='text/plain')

# Inicialização do app:
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8282)
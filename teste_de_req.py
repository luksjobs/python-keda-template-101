import requests

url = "http://localhost:8282/metrics"

num_requests = 1000

for i in range(num_requests):
    try:
        response = requests.get(url)

        if response.status_code == 200:
            print(f"solicitacao {i+1}: Sucesso!")
        else:
            print(f"Solicitacao {i+1}: Falha> {response.status_code}")
    except Exception as e:
        print(f"Solicitacao {i+1}: Erro {e}")